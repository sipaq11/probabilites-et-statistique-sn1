%%% Copyright (C) 2023 Simon Paquette et Pascal Turbis
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Sommation} \label{annexeSommation}
%
%
\begin{bloc}{m}
Nous présentons dans cette annexe une notation qui permet d'écrire de façon succincte les additions. 
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{defi}[Sigma]{defSigma}
Nous utilisons le symbole \(\Sigma\) pour représenter une somme. Ce symbole est la lettre grecque \textit{sigma} en majuscule.\index{Symbole Sigma}
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
Ainsi, lorsque nous rencontrons le symbole \(\Sigma\), nous savons qu'il s'agit d'une addition. 

\begin{exemple}
\QS{Dire ce que signifie \(\dsum{}{}{5}\)\ .}{

Il s'agit d'une somme du nombre \(5\) un certain nombre de fois. C'est donc quelque chose comme \(5+5+5+\dots+5\). 
}
\end{exemple}

Le problème dans l'exemple précédent est que nous ne savons pas combien de \(5\) sont additionnés. Il faut donc améliorer la notation. Nous utiliserons donc une variable qui servira de compteur pour savoir combien de fois l'addition est faite. Souvent, la variable utilisée est notée \(i\), \(j\) ou \(k\). Son nom n'a pas d'importance. Cette variable rend des valeurs entières, le plus souvent des nombres naturels. On fixe une valeur de départ et pour chaque addition la variable s'incrémente d'une unité, c'est-à-dire qu'on lui ajoute \(1\). Ainsi, on pourrait avoir au départ \(i=0\) et s'il faut faire trois additions, la variable terminera à \(2\) : une addition quand \(i=0\), une quand \(i=1\) et une dernière quand \(i=2\). L'exemple suivant montre comment nous inscrivons la variable compteur dans le symbole \(\Sigma\).

\begin{exemple}
\QS{Écrire \(5+5+5+5+5+5+5\) à l'aide du symbole de sommation.}{
\[5+5+5+5+5+5+5 = \dsum{i=0}{6}{5}\]%
Ainsi, une copie de la valeur \(5\) est présente dans la somme pour chaque valeur que le compteur \(i\) prend, c'est-à-dire \(0\), \(1\), \(2\), \(3\), \(4\), \(5\) et \(6\). Remarquons qu'il y a plusieurs réponses possibles.%
\[5+5+5+5+5+5+5 = \dsum{i=4}{10}{5}\]%
La somme est équivalente puisque nous additionnons le nombre \(5\) pour chaque valeur que prend le compteur, c'est-à-dire \(4\), \(5\), \(6\), \(7\), \(8\), \(9\) et \(10\).
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\setbeamercovered{invisible} 
\begin{defi}[Terme général, bornes et indice]{defIndice}
Pour la somme \(\dsum{i=r}{s}{a_i}\), nous appellerons \(a_i\) le \defTerme{terme général}, \(r\), la \defTerme{borne inférieure}, \(s\), la \defTerme{borne supérieure} et \(i\) sera l'\defTerme{indice de sommation}. \index{Terme général!sommation} \pause

Les bornes \(r\) et \(s\) doivent être des nombres entiers tels que \(r \leq s\). 
\end{defi}
\dans{n}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
Comme nous avons pu le constater dans la définition précédente, le terme général n'est pas nécessairement une constante. Il peut s'agir d'une expression qui dépend de la valeur de l'indice de sommation. Dans les exemples qui suivent, nous donnerons deux réponses possibles lorsqu'il y en a plusieurs, mais il y en a bien d'autres.

\begin{exemple}
\QS{Écrire \(2+3+4+5+6+7+8+9\) à l'aide du symbole de sommation.}{
\[2+3+4+5+6+7+8+9 = \dsum{i=2}{9}{i} = \dsum{i=0}{7}{(i+2)}\]%
}
\end{exemple}

\begin{exemple}
\Q{Développer les sommations suivantes.}
\debutSousQ
\QS{\(\dsum{i=1}{4}{\paren{3i+2}}\)}{

\(5+8+11+14\)}
\QS{\(\dsum{k=2}{5}{\dfrac{2^k}{10k}}\)}{

\(\dfrac{4}{20} + \dfrac{8}{30} + \dfrac{16}{40} + \dfrac{32}{50}\)}
\finSousQ
\end{exemple}

\begin{exemple}

\Q{Écrire les expressions suivantes à l'aide du symbole de sommation.}
\debutSousQ
\QS{\(1+3+5+7+9+11\)}{

\(\dsum{i=1}{6}{(2i-1)}=\dsum{i=0}{5}{(2i+1)}\)}
\QS{\(1+2+4+8+16+32+64+128\)}{

\(\dsum{k=0}{7}{2^k} = \dsum{k=1}{8}{2^{k-1}}\)}
\QS{\(\dfrac{2}{4} - \dfrac{3}{7} + \dfrac{4}{10} - \dfrac{5}{13}+\dots+\dfrac{22}{64}\)}{

\(\dsum{j=2}{22}{\dfrac{(-1)^j j}{3j-2}} = \dsum{j=0}{20}{\dfrac{(-1)^j (j+2)}{3j+4}}\)}
\finSousQ
\end{exemple}

Voyons maintenant quelques propriétés des sommations qui nous faciliteront leurs manipulations.
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thm}[Propriétés des sommations]{thmPropSom}
Soit \(r\), \(k\) et \(n \in \mathds{Z}\) tels que \(r \leq n\). Soit \(c \in \mathds{R}\). \index{Théorème!Propriétés des sommations}
\begin{liste}[beaOpt=<+- |alert@+>, itSep=0.3cm]
\item \(\dsum{i=r}{n}{\paren{a_i \pm b_i}} = \dsum{i=r}{n}{a_i} \pm \dsum{i=r}{n}{b_i}\) 
\dans{n}{\vfill}
\item \(\dsum{i=r}{n}{\paren{c\cdot a_i}} = c \cdot \dsum{i=r}{n}{a_i}\)  
\dans{n}{\vfill}
\item \(\dsum{i=r}{n}{a_i} = \dsum{i=r}{k}{a_i} + \dsum{i=k+1}{n}{a_i}\)   
\end{liste}
\end{thm}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
La première propriété est simple à comprendre et à expliquer. Il s'agit d'utiliser la commutativité de l'addition dans les nombres réels pour regrouper les termes souhaités et d'utiliser l'associativité de l'addition dans les nombres réels pour nous permettre de faire les deux groupes d'additions.

La deuxième propriété est simplement une mise en évidence du nombre réel \(c\) dans la sommation.

La troisième propriété nous indique qu'il est possible d'arrêter la sommation avant la fin, puis de la terminer dans une deuxième sommation.
\begin{exemple}
\Q{Si nous désirons additionner les cent premiers nombres naturels, il est possible d'additionner les trente premiers nombres, ensuite d'additionner les soixante-dix nombres restants pour finalement additionner les deux résultats.%
\begin{align*}
\dsum{i=1}{100}{i} &= 1 + 2 + 3 + \dots + 99 + 100 \\
&= (1+2+\dots+30) + (31 + 32+\dots+100)  \\
&= \dsum{i=1}{30}{i} + \dsum{i=31}{100}{i}
\end{align*}
}%
\end{exemple}

Les formules suivantes nous permettrons d'évaluer facilement certaines sommations.
\end{bloc}%m
%
%
\begin{bloc}{bimn}[Fournies aux examens]
\begin{prop}[Formules de sommation]{propFormulesSom}
Soit \(n \in \mathds{N}^*\).
\begin{liste}[beaOpt =<+- |alert@+>, itSep=\dans{b}{0.3cm} ]
\item \(\dsum{i=1}{n}{i} = \dfrac{n(n+1)}{2}\) 
\item \(\dsum{i=1}{n}{i^2} = \dfrac{n(n+1)(2n+1)}{6}\) 
\item \(\dsum{i=1}{n}{i^3} = \dfrac{n^2(n+1)^2}{4}\) 
\end{liste}
\index{Proposition!formules de sommation}
\preuve{%

Nous démontrons uniquement la première formule.

Posons \(\dsum{i=1}{n}{i} = S\). Nous souhaitons trouver la valeur de \(S\).%
\begin{align*}
& \ S = 1 + 2 + 3 + \dots + (n-1) + n \\
\shortintertext{Nous pouvons aussi écrire cette sommation en inversant l'ordre. La somme est évidemment la même.}
& \ S = n + (n-1) + (n-2) + \dots + 2 + 1  \\
\shortintertext{Maintenant, nous allons additionner les deux côtés des deux équations.}
\Rightarrow \ & 2S = (n+1) + (n+1) + (n+1) + \dots + (n+1) + (n+1)\\
\shortintertext{Il y a autant de \((n+1)\) que de termes dans la sommation initiale, c'est-à-dire \(n\) termes.}
\Rightarrow \ & 2S = n(n+1)\\
\Rightarrow \ & \ S = \dfrac{n(n+1)}{2}
\end{align*}
\finDemo
}

\end{prop}
\dans{n}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
Remarquons que les formules précédentes sont bonnes même si la borne inférieure de sommation vaut \(0\). En effet, lorsque \(i=0\), nous ajoutons la valeur \(0\) à la somme, ce qui ne change rien à la valeur finale.

Les exemples suivants illustrent l'utilisation des propriétés et des formules précédentes.
\begin{exemple}
\Q{Évaluez les sommations suivantes.}
\debutSousQ
\QS{\(\dsum{i=1}{10}{i}\)}{%

Nous appliquons directement la première formule.
\[\dsum{i=1}{10}{i} = \dfrac{10\cdot 11}{2} = 55\]}
\QS{\(\dsum{i=6}{34}{i^2}\)}{%

Pour utiliser les formules, la sommation doit débuter à \(0\) ou à \(1\). Pour y arriver, nous utilisons la propriété 3 du \infobulle{théorème \ref{thmPropSom}}{thmPropSom} vu précédemment.%
\[\dsum{i=6}{34}{i^2} = \dsum{i=1}{34}{i^2} - \dsum{i=1}{5}{i^2}\]%
Ensuite, nous pouvons utiliser la deuxième \infobulle{formule}{propFormulesSom} de sommation.%
\[\dsum{i=6}{34}{i^2} = \dfrac{34\cdot 35\cdot (2\cdot 34 +1)}{6} - \dfrac{5\cdot 6\cdot (2\cdot 5 +1)}{6} = \num{13630}\]}%
\QS{\(\dsum{i=0}{20}{(2i^3+4)}\)}{%

Nous utilisons d'abord la propriété 1 du \infobulle{théorème \ref{thmPropSom}}{thmPropSom}.%
\[\dsum{i=0}{20}{(2i^3+4)} = \dsum{i=0}{20}{2i^3} + \dsum{i=0}{20}{4}\]%
Ensuite, nous appliquons la propriété 2 du \infobulle{théorème \ref{thmPropSom}}{thmPropSom}.%
\[\dsum{i=0}{20}{(2i^3+4)} = 2\dsum{i=0}{20}{i^3} + \dsum{i=0}{20}{4}\]%
Finalement, nous pouvons utiliser la deuxième \infobulle{formule}{propFormulesSom} de sommation. Pour la sommation de la constante, nous remarquons que la valeur \(4\) est additionnée \(21\) fois.%
\[\dsum{i=0}{20}{(2i^3+4)} = 2\cdot\frac{20^2\cdot 21^2}{4} + 21 \cdot 4 = \num{88284}\]%
}%
\finSousQ
\end{exemple}
\end{bloc}%m
%
%
