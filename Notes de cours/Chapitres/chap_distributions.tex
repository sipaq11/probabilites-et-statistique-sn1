%%% Copyright (C) 2022-2023 Simon Paquette et Pascal Turbis
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\dans{n}{\newpage}
\chapter{Lois de probabilité}
%
%
%%%\dans{mn}{%
%%%\DraftwatermarkOptions{firstpageonly = false, stamp=true, angle=45, hpos=s 0.5\paperheight, vanchor=m, alignement=c, lightness = 0.5, text="À compléter"}
%%%}
%
%
\section{Variable aléatoire}
%
%
\begin{bloc}{bimn}
\begin{defi}[Variable aléatoire]{defVarAlea}
Une \defTerme{variable aléatoire} quantitative est une variable qui associe un nombre à tout résultat possible d'une expérience aléatoire. On la désigne habituellement par les dernières lettres de l'alphabet en majuscules (\(X\), \(Y\), \(Z\)).
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{liste}[type=itemize, beaOpt=<+-| alert@+>, itSep=\dans{b}{0.4cm}]
\item Le \defTerme{champ} d'une variable aléatoire \(X\), noté \(\CH\paren*{X}\), est l'ensemble des valeurs possibles de \(X\).

\item Une variable aléatoire \defTerme{discrète} est une variable aléatoire dont le champ est un ensemble fini ou dénombrable. Dans le cas contraire, elle est \defTerme{continue}.
\end{liste}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}
\begin{exemple}
\begin{liste}[beaOpt=<+-| alert@+>, itSep=\dans{b}{0.4cm}]
\item Le nombre d'enfants d'une personne choisie au hasard.  

\item Le nombre de lancers d'un dé nécessaires avant d'obtenir le chiffre 4. 

\item La taille d'une personne choisie au hasard.
\end{liste}
\end{exemple} 
\end{bloc}%bn
%
%
\section{Variable aléatoire discrète}
%
%
\begin{bloc}{bimn}
\begin{defi}[Loi de probabilité]{desLoiProb}
La \defTerme{loi de probabilité} (ou fonction de probabilité) d'une variable aléatoire discrète \(X\) est la fonction \(p(x)\) définie sur \(\CH\paren*{X}\) telle que \(p(a) = P\paren*{X = a}\).
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}[Loi de probabilité]

\begin{tabular}{cccc}
\(p\) : & \(\CH(X)\) & \(\rightarrow\) & \([0, 1]\) \\
      & \(x_i\)   & \(\mapsto \)    & \(p(x_i) = P(X=x_i)\) 
\end{tabular}
\dans{n}{\newpage}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}[Représentation d'une loi de probabilité discrète]

La \defTerme{distribution de probabilité de \(X\)} est le tableau qui associe à chaque valeur de \(X\) sa probabilité. 
\begin{center}
\renewcommand{\arraystretch}{\dans{b}{2}\dans{n}{1}}
\begin{tabular}{l|c}
\(x_i\) & \(p(x_i)\) \\ \hline
\(x_1\) & \(p(x_1) = P(X = x_1)\) \\
\(\vdots\) & \(\vdots\) 
\end{tabular}
\renewcommand{\arraystretch}{\dans{b}{1}\dans{n}{1}}
\end{center}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{On lance deux dés équilibrés. Soit \(X\) la variable aléatoire représentant la somme des deux résultats obtenus. Donner la loi de probabilité de la variable \(X\).}{
\begin{minipage}{\textwidth/2}
\begin{tabular}{l|c}
\(x_i\) & \(P(X = x_i)\) \\ \hline
2 & \(\frac{1}{36}\) \\
3 & \(\frac{2}{36}\) \\
4 & \(\frac{3}{36}\) \\
5 & \(\frac{4}{36}\) \\
6 & \(\frac{5}{36}\) \\
7 & \(\frac{6}{36}\) 
\end{tabular}
\end{minipage}
\begin{minipage}{\textwidth/2}
\begin{tabular}{l|c}
\(x_i\) & \(P(X = x_i)\) \\ \hline
8 & \(\frac{5}{36}\) \\
9 & \(\frac{4}{36}\) \\
10 & \(\frac{3}{36}\) \\
11 & \(\frac{2}{36}\) \\
12 & \(\frac{1}{36}\) 
\end{tabular}
\end{minipage}
}[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}[Remarque]

Selon les axiomes des probabilités, nous savons que\dans{b}{\\} \(0\leq p(x_i)\leq 1\) et que \(\dsum{x_i \in \CH\paren*{X}}{}{p(x_i)} = 1\).
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{defi}[Fonction de répartition]{defFctRepartition}
La \defTerme{fonction de répartition} (ou fonction cumulative des probabilités) d'une variable aléatoire \(X\) est la fonction \(F(x)\) telle que \(F(a) = P\paren*{X \leq a}\).
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}[Remarque]

On peut calculer \(P\paren*{X = a}\) en notant que \(P\paren*{X = a} = P\paren*{X \leq a} - P\paren*{X < a} = F(a) - F(b)\), où \(b\) est la valeur possible de \(X\) immédiatement inférieure à \(a\) dans le champ de \(X\).
\dans{n}{\newpage}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Un couple décide d'avoir des enfants jusqu'à ce qu'il ait un enfant de chaque sexe ou trois enfants au maximum. Soit \(X\), la variable aléatoire qui indique le nombre de filles que pourrait avoir ce couple. Déterminer le champ de \(X\), la loi de probabilité de \(X\) et la fonction de répartition de \(X\).}{
\(\CH\paren*{X} =\{0, 1, 2, 3\}\) 

\begin{tabular}{l|c}
\(x_i\) & \(P(X = x_i)\) \\ \hline
0 & \(\frac{1}{8}\) \\
1 & \(\frac{5}{8}\) \\
2 & \(\frac{1}{8}\) \\
3 & \(\frac{1}{8}\) 
\end{tabular}
\hspace*{1cm}
\(
F(x) =
\begin{cases}
\frac{1}{8} & \text{si } x = 0\\
\frac{3}{4} & \text{si } x = 1\\
\frac{7}{8} & \text{si } x = 2\\
1 & \text{si } x = 3
\end{cases}
\)
}[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Soit \(X\), une variable aléatoire discrète définie sur \(\mathds{Z}\) dont la fonction de répartition est \\
\(
F(x) =
\begin{cases}
0 & \text{si } x < 1\\
0,7 & \text{si } x=1\\
0,9 & \text{si } x=2\\
1 & \text{si } x \geq 3
\end{cases}
\).

Déterminer la loi de probabilité de \(X\), ainsi que la valeur des probabilités \(P\paren*{X \leq 2}\), \(P\paren*{X > 7}\) et \(P\paren*{1< X< 5}\).}{
\begin{tabular}{l|c}
\(x_i\) & \(P(X = x_i)\) \\ \hline
1 & \(0,7\) \\
2 & \(0,2\) \\
3 & \(0,1\) \\
sinon & \(0\) 
\end{tabular}
\hspace{1cm}
\begin{minipage}[t]{\textwidth/2}
\(P\left( X \leq 2\right) = 0,9\)\\ 
\(P\paren*{X > 7} = 0\)\\ 
\(P\paren*{1< X< 5}=0,3\)
\end{minipage}
}[\vfill \newpage]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{defi}[Espérance mathématique]{defEspMatDiscrete}
L'\defTerme{espérance mathématique} d'une variable aléatoire discrète \(X\), notée \(\E\paren*{X}\), \(\mu\) ou \(\mu_X\), est définie par
\[\E\paren*{X} = \dsum{x_i\in \CH\paren*{X}}{}{x_i \cdot P\paren*{X = x_i}} = \dsum{x_i \in \CH\paren*{X}}{}{x_i \cdot p(x_i)}\text{ ,}\]
où \(p(x)\) est la loi de probabilité de \(X\).
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}[Interprétation]

L'\defTerme{espérance mathématique} de \(X\) est la moyenne des valeurs prises par la variable \(X\) si l'expérience aléatoire est répétée un nombre infini de fois. 
\end{bloc}%bn


\begin{bloc}{bimn}
\begin{defi}[Variance]{defVarianceMatDiscrete}
La \defTerme{variance} d'une variable aléatoire discrète \(X\), notée \(\Var\paren*{X}\), \(\sigma^2\) ou \(\sigma^2_X\), est définie par
\begin{align*}
\Var\paren*{X} &= \dsum{x_i \in \CH\paren*{X}}{}{(x_i-\mu)^2 \cdot P\paren*{X = x_i}}\\ \pause
&= \dsum{x_i\in \CH\paren*{X}}{}{(x_i-\mu)^2 \cdot p(x_i)}\text{ ,}%\\ \pause
%%&= \E\crochets*{\paren*{X-\mu}^2}
\end{align*}
où \(p(x)\) est la loi de probabilité de \(X\).
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{defi}[Écart type]{defEcartTypeVarAlea}
L'\defTerme{écart type} d'une variable aléatoire \(X\), noté \(\sigma\) ou \(\sigma_X\), est défini par \(\sigma = \sqrt{\Var\paren*{X}}\).
\end{defi}
\end{bloc}%bimn


\begin{bloc}{bn}
\begin{exemple}

\QR{Un couple décide d'avoir des enfants jusqu'à ce qu'il ait un enfant de chaque sexe ou trois enfants au maximum. Soit \(X\), la variable aléatoire qui indique le nombre de filles que pourrait avoir ce couple. Déterminer l'espérance et la variance de \(X\).}{\(\E\paren*{X} = \dfrac{5}{4}\) fille et \(\Var\paren*{X}=\dfrac{11}{16}\) fille\(^2\)}[\vfill \newpage]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Une urne contient cinq boules rouges, cinq boules blanches et cinq boules jaunes. On tire cinq boules de l'urne sans remise. Soit \(X\), le nombre de boules jaunes parmi les cinq boules tirées. Déterminer l'espérance et l'écart type de \(X\).}{\(\E\paren*{X} = \dfrac{5}{3}\) et \(\sigma_X\approx 0,8909\) }[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{\only<1-2>{Quelle est l'espérance de gain au 6/49? \dans{b}{\\[0.5cm] \pause}
\begin{center}
\renewcommand{\arraystretch}{\dans{b}{1.5}\dans{n}{1}}
\begin{tabular}{c|c|c}
Catégorie  & Lot   &       Chances de gagner \\
6/6   &  \num{8000000} \$   &   1 / \num{13983816} \\
5/6 * &   \num{96000} \$ & 1 / \num{2330636} \\
5/6  &   \num{2200} \$ & 1 / \num{55492} \\
4/6  &   80 \$    &  1 / \num{1033}  \\
3/6  &   10 \$  &    1 / \num{56,7}  \\
2/6 * &   5 \$   &    1 / \num{81,2} \\ 
2/6  &   3 \$   &    1 / \num{8,3} \\ \hline
TOTAL*   &  &    1 / \num{6,6}       \\
\end{tabular}
\renewcommand{\arraystretch}{\dans{b}{1}\dans{n}{1}}
\end{center}}}[\vfill \newpage]
\dans{b}{\only<3>{\(\E\paren*{X} \approx 1,33 \$\)} \only<4>{\(\E\paren*{X} \approx 0,88 \$\)} \only<5-6>{\(\E\paren*{X} \approx -1,67 \$\)} \hspace*{2cm} \only<6>{\(\Var\paren*{X} \approx \num{4580768},35 \$\)}}
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{prop}[Propriétés de l'espérance]{propPropEsp}
Soit \(X\) et \(Y\) des variables aléatoires. \dans{i}{\\}
\begin{liste}[beaOpt=<+-| alert@+>, itSep=\dans{b}{0.4cm}]
\item \(\E\paren*{X \pm Y} = \E\paren*{X} \pm \E\paren*{Y}\)
\item Si \(a\) et \(b\) sont des nombres réels, alors\dans{b}{\\} \(\E\paren*{aX+b} = a\E\paren*{X} + b\).
\item Si \(X\) et \(Y\) indépendantes, alors \(\E\paren*{X \cdot Y} = \E\paren*{X} \cdot \E\paren*{Y}\).
\end{liste}
\end{prop}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Le temps d'attente à l'aéroport varie selon le temps passé à enregistrer les bagages et le temps passé avec les douaniers. On sait que le temps passé à enregistrer les bagages est en moyenne de 32 minutes et celui passé avec les douaniers est en moyenne de 15 minutes. Quel est le temps moyen d'attente à l'aéroport?}{Le temps d'attente moyen est de 47 minutes.} [\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Le nombre de commandes quotidiennes \(X\) reçues par le service d'expédition d'une compagnie admet la loi de probabilité \(f\) donnée par \(f(5) = 0,1\), \(f(15) = 0,4\) et \(f(25) = 0,5\). Si chaque commande coûte à la compagnie 350\$ et que les frais fixes pour chaque jour sont de \num{2750}\$, quel est le profit quotidien moyen de cette compagnie lorsque le prix de vente est de 520\$ par commande?}{Le profit quotidien moyen de cette compagnie lorsque le prix de vente est de 520\$ par commande est de 480\$ par jour.}[\vfill \newpage] 
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{prop}[Propriétés de la variance]{propPropVar}
Soit \(X\) et \(Y\) deux variables aléatoires. \dans{i}{\\}
\begin{liste}[beaOpt=<+-| alert@+>, itSep=\dans{b}{0.4cm}]
\item \(\Var\paren*{X} = \E\paren*{X^2} -\paren[Big]{\E\paren*{X}}^2\)
\item Si \(a\) et \(b\) sont des nombres réels, alors\dans{b}{\\} \(\Var\paren*{aX +b} = a^2 \Var\paren*{X}\).
\item \(\Var\paren*{X \pm Y} = \Var\paren*{X} \pm \Var\paren*{Y}\) si \(X\) et \(Y\) sont indépendantes.
\end{liste}
\end{prop}
\end{bloc}%bimn
%
% #5 retiré
\devoirs[pageslecture={151 à 160 et 175 à 179}, pagesexe={180 et 181}, numsexe={1 - 2 - 3 - 4 - 6ab - 7 - 10}]
%
%
\section[Lois aléatoires discrètes]{Lois (ou modèles) aléatoires discrètes}
%
%
\subsection{Loi uniforme discrète}
%
%
\begin{bloc}{bimn}
\begin{defi}[Loi uniforme discrète]{defLoiUniformeDiscrete}
Soit \(X\) une variable aléatoire discrète avec\dans{b}{\\} \(\CH(X) = \{x_1, x_2,\dots, x_n\}\). 

On dit que \(X\) suit une \defTerme{loi uniforme}, noté \(X \sim U(n)\), si pour tout \(x_i \in \CH(X)\), on a \(p(x_i) = P(X=x_i) = \dfrac{1}{n}\). 
\end{defi}
\pause
 
Pour chaque expérience aléatoire dont les événements atomiques sont équiprobables, nous avons une loi de probabilité uniforme. 
\end{bloc}%bimn


\begin{bloc}{bn}[Caractéristiques]

Soit \(X\) une variable aléatoire telle que \(X \sim U(n)\). \pause

\[\E(X) = \pause \dsum{i=1}{n}{\paren*{x_i\cdot \frac{1}{n}}} = \dfrac{1}{n}\cdot \dsum{i=1}{n}{x_i}\] \pause

\[\Var(X) = \pause \sigma^2 = \dfrac{\dsum{i=1}{n}{(x_i - \E(X))^2}}{n}\]
\dans{n}{\newpage}
\end{bloc}%bn
%
%
\subsection{Loi binomiale}
%
%
\begin{bloc}{bn}
\begin{exemple}
\debutSousQ[beaOpt=<+-| alert@+>, itSep=\dans{b}{0.3cm}]
\Q{Dans un groupe de 50 personnes, il y a 30 adultes et 20 enfants. Si on pige avec remise 10 personnes, combien aurons-nous pigé d'enfants ?} 

\Q{Sur une chaîne de montage nous connaissons la probabilité \(p\) qu'une pièce soit défectueuse. Si nous examinons 100 pièces, combien d'entre elles devraient être défectueuses?}

\Q{Une urne contient 10 boules blanches, 20 bleues et 50 rouges. Si on pige 10 boules avec remise, combien de boules blanches devrait-on obtenir?}[\vfill] 
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}[Épreuve de Bernouilli]

Une \defTerme{épreuve de Bernouilli} est un processus visant à vérifier la réalisation ou la non-réalisation d'un événement fixé au préalable. \dans{b}{\\[0.4cm] \pause} 

La réalisation de cet événement est appelé \og succès \fg, tandis que la non-réalisation de cet événement est appelé \og échec \fg. \dans{b}{\\[0.4cm] \pause} 

Par convention, la probabilité d'un succès est notée \(p\) et la probabilité d'un échec \(q = 1-p\).
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{defi}[Loi binomiale]{defLoiBin}
Soit une expérience aléatoire qui consiste à répéter, dans les mêmes conditions, \(n\) épreuves de Bernouilli de probabilité de succès \(p\). \pause

Soit \(X\) la variable aléatoire qui compte le nombre de succès obtenus à la suite de ces \(n\) épreuves. \pause

On dit alors que \(X\) suit une \defTerme{loi binomiale} de paramètres \(n\) et \(p\). \pause

On note \(X \sim B(n; p)\).
\end{defi}
\dans{n}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}[Loi de probabilité]

Trouver la fonction de probabilité d'une variable aléatoire \(X\) telle que \(X \sim B(n; p)\).  \dans{b}{\\[0.4cm] \pause} \dans{n}{\vfill}

Vérifier que \(0 \leq P(X=k) \leq 1\) et que \(\dsum{k=0}{n}{P(X=k)} = 1\). \dans{n}{\vfill \newpage}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}
\debutSousQ[beaOpt=<+-| alert@+>, itSep=\dans{b}{0.3cm}]
\QR{Si \(X \sim B(10; 0,25)\), calculer \(P(X = 3)\).}{\(P(X = 3) = \binom{10}{3}\cdot 0,25^3\cdot 0,75^7 \approx 0,2503\)} \dans{n}{\vfill}
\QR{Si \(X \sim B(5; 0,2)\), calculer \(P(X < 3)\).}{\(P(X < 3) = \binom{5}{0}\cdot 0,2^0\cdot 0,8^5 + \binom{5}{1}\cdot 0,2^1\cdot 0,8^4 + \binom{5}{2}\cdot 0,2^2\cdot 0,8^3 = 0,94208\)} \dans{n}{\vfill}
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Dans une grande école, \qty{35}{\%} des étudiants fument. On choisit 14 étudiants au hasard.}
\debutSousQ[beaOpt=<+- |alert@+>, itSep=\dans{b}{0.4cm}] 
\QR{Quelles sont les chances que huit étudiants sur les 14 fument?}{\(P(X=8) = \binom{14}{8}\cdot 0,35^8 \cdot 0,65^6 \approx 0,051 \)} \dans{n}{\vfill}
\QR{Quelles sont les chances qu'il y ait au moins un non-fumeur dans l'échantillon?}{\(P(Y\geq 1) = P(X\leq 13) = 1- P(X=14) = 1-0,35^{14} \approx 0,\num{999999586} \)} \dans{n}{\vfill \newpage}
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{lem}[]{lemIndicatrice}
Si \(X \sim B(1; p)\), alors \(\E(X) = p\) et \(\Var(X) = pq\).
\end{lem}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{thm}[]{thmEspVarBin}
Si \(X \sim B(n; p)\), alors \(\E(X) = np\) et \(\Var(X) = npq\).
\end{thm}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{thm}[]{thmSomBin}
Si \(X \sim B(n_1; p)\) et \(Y \sim B(n_2; p)\), alors\dans{b}{\\} \(X+Y \sim B(n_1+n_2; p)\).
\end{thm}
\end{bloc}%bimn
%
% #7-8 retirés
\devoirs[pageslecture={183 à 198}, pagesexe={218 et 219}, numsexe={1 - 2 - 3 - 4 - 5 - 6},commentaire1={Revoir l'intégration par parties et les intégrales impropres.}]
\dans{n}{\newpage}
%
%
%%%\subsection{Loi géométrique}
%%%
%%%\begin{bloc}{bn}
%%%\begin{defi}[Loi géométrique]{defLoiGeo}
%%%Soit une expérience aléatoire qui consiste à répéter une épreuve de Bernouilli, de probabilité de succès \(p\), dans les mêmes conditions. \pause
%%%
%%%Soit \(X\) la variable aléatoire qui compte le nombre d'épreuves de Bernouilli nécessaire pour obtenir un premier succès. \pause
%%%
%%%On dit alors que \(X\) suit une \emph{loi géométrique} de paramètre \(p\). \pause
%%%
%%%On note \(X\sim Geo(p)\).
%%%\end{defi}
%%%\end{bloc}%bn
%%%
%%%
%%%\begin{bloc}{bn}[Loi de probabilité}
%%%
%%%Trouver la fonction de probabilité d'une variable aléatoire telle que \(X\sim Geo(p)\).  \dans{b}{\\[0.4cm] \pause} \dans{n}{\vfill}
%%%
%%%Vérifier que $\dsum{k=0}{n}{P(X=k)} = 1$ et que $0 \leq P(X=k) \leq 1$.
%%%\dans{n}{\vfill \newpage}
%%%\end{bloc}%bn
%%%
%%%
%%%
%%%\begin{bframe}{Loi de Poisson}
%%%
%%%On s'intéresse au nombre de fois qu'un événement se produise (comme pour la loi binomiale) mais dans un intervalle de temps continu plutôt qu'un nombre fini d'épreuves. \\[0.5cm] \pause 
%%%
%%%Exemple : nombre d'accidents de la route dans une semaine.
%%%\end{bframe}
%
%
\section{Variable aléatoire continue}
%
%
\begin{bloc}{bimn}
\begin{defi}[Fonction de densité]{defFctDensite}
La fonction $f$ est une \emph{fonction de densité} si 

\begin{liste}[type=itemize, itSep=\dans{b}{0.5cm}, beaOpt=<+- |alert@+>]
\item \(f(x) \geq 0\) pour tout \(x\) et
\item \(\intd{-\infty}{\infty}{f(x)}{x} = 1\).
\end{liste}
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Déterminer si les fonctions suivantes sont des fonctions de densité.} 
\debutSousQ[itSep=\dans{b}{0.5cm}, beaOpt=<+- |alert@+>]
\QR{\(f(x) = \begin{cases} 3x^2 & \quad\text{si } 0\leq x \leq 1 \\ 0 & \quad\text{sinon} \end{cases}\)}{Oui.}  \dans{n}{\vfill}
\QR{\(g(x) = \begin{cases} e^x & \quad\text{si } 1 < x < 3 \\ 0 & \quad\text{sinon} \end{cases}\)}{Non, car \(\intd{-\infty}{\infty}{g(x)}{x} \neq 1\).} \dans{n}{\vfill \newpage}
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
La variable aléatoire continue \(X\) a \(f\) comme fonction de densité si
\[P(a\leq X \leq b) = \intd{a}{b}{f(x)}{x} \text{ .}\]
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Si \(X\) a comme fonction de densité \(f(x) = \begin{cases} \frac{1}{x^2} & \quad\text{si } x \geq 1 \\ 0 & \quad\text{si } x<1 \end{cases}\), calculer les probabilités suivantes.}
\debutSousQ[itSep=\dans{b}{0.5cm}, beaOpt=<+- |alert@+>]
\QR{\(P(1 \leq X \leq 2)\)}{\(\frac{1}{2}\)} \dans{n}{\vfill}
\QR{\(P(X > 3)\)}{\(\frac{1}{3}\)} \dans{n}{\vfill \newpage}
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{defi}[Fonction de répartition]{desFctRepartition}
Soit \(X\) une variable aléatoire ayant la fonction de densité \(f\). 

On appelle \defTerme{fonction de répartition} de \(X\) la fonction \(F\) définie par\dans{n}{\\} \(F(x) = P(X\leq x) = \intd{-\infty}{x}{f(t)}{t}\).
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}[Remarques]

Soit \(X\) une variable aléatoire de fonction de densité \(f\) et de fonction de répartition \(F\).
\begin{liste}[itSep=\dans{b}{0.5cm}, beaOpt=<+- |alert@+>]
\item \(P(X=a) = \intd{a}{a}{f(x)}{x}=0\)
\item \(P(a<X<b) = P(a\leq X \leq b)\)
\item \(P(a\leq X \leq b) = \intd{a}{b}{f(x)}{x} = F(b)-F(a)\)
\end{liste}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{La durée de vie (en années) d'un téléviseur d'une certaine marque suit une loi continue dont la fonction de densité est
\[f\left( x\right) = \begin{cases} \frac{1}{15}e^{-x/15} & \text{pour } x>0\\ 0 & \text{sinon}\end{cases}\text{ .}\]}
\debutSousQ[itSep=\dans{b}{0.5cm}, beaOpt=<+- |alert@+>]
\QR{Déterminer la fonction de répartition.}{\(F(x) = 1-e^{-x/15}\)}[\newpage]
\QR{Déterminer la probabilité qu'un téléviseur de cette marque choisi au hasard ait une durée de vie entre six ans et dix ans.}{\(F(10)-F(6) \approx 0,1569\)}[\vfill] 
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{defi}[Espérance mathématique]{defEspMatContinue}
L'\emph{espérance mathématique} d'une variable aléatoire continue \(X\), notée \(\E(X)\), est définie par
\[\E(X) = \intd{-\infty}{\infty}{x\cdot f(x)}{x}\text{ ,}\]
où \(f(x)\) est la fonction de densité de \(X\).
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}[Propriétés de l'espérance]

Les	\infobulle{propriétés}{propPropEsp} de l'espérance d'une variable aléatoire continue sont les mêmes que pour une variable discrète.
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{defi}[Variance]{defVarianceContinue}
La \defTerme{variance} d'une variable aléatoire continue \(X\), notée \(\Var(X)\), est définie par
\[\Var(X) = \intd{-\infty}{\infty}{(x - \E(X))^2\; f(x)}{x}\text{ ,}\]
où \(f(x)\) est la fonction de densité de \(X\).
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}[Propriétés de la variance]

Les \infobulle{propriétés}{propPropVar} de la variance d'une variable aléatoire continue sont les mêmes que pour une variable discrète.
\dans{n}{\newpage}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Soit \(X\), une variable aléatoire continue dont la fonction de densité est donnée par
\[f(x) = \begin{cases} 3(1-x)^2 & \text{pour } 0 < x < 1\\ 0 & \text{sinon}\end{cases}\text{ .}\]
Déterminer l'espérance et la variance de \(X\).}{\(\E(X) = \frac{1}{4}\) et \(\Var(X) = 0,0375\)}[\vfill] 
\end{exemple}
\end{bloc}%bn
%
%
\devoirs[pageslecture={222 à 229}, pagesexe={230 et 231}, numsexe={1 - 2 - 3 - 4 - 5 - 6}]
\dans{n}{\newpage}
%
%
\section[Lois aléatoires continues]{Lois (ou modèles) aléatoires continues}
%
%
\subsection{Loi uniforme continue}
%
%
\begin{bloc}{bimn}
\begin{defi}[Loi uniforme continue]{defLoiUniformeContinue}
Soit \(X\) une variable aléatoire continue. On dira que \(X\) suit une \defTerme{loi uniforme} sur l'intervalle \(]a, b[\) si sa fonction de densité est \dans{b}{\\} \(f(x) = \begin{cases} \dfrac{1}{b-a} & \quad\text{pour } x \in ]a, b[ \\ 0 & \quad\text{sinon} \end{cases}\). 

On écrit \(X \sim U(a, b)\).
\end{defi}
\end{bloc}%bimn


\begin{bloc}{bn}[Caractéristiques]

Soit \(X\) une variable aléatoire telle que \(X \sim U(a, b)\). \pause

Déterminer la fonction de répartition de \(X\), son espérance ainsi que sa variance. \pause
\dans{b}{\[F(x) = \begin{cases} 0 & \quad\text{si } x \leq a \\ \dfrac{x-a}{b-a} & \quad\text{si } a < x < b \\ 1 & \quad\text{si } x\geq b \end{cases}\] \pause
\[\E(X) = \pause \dfrac{a+b}{2}\] \pause
\[\Var(X) = \pause \dfrac{(b-a)^2}{12}\]}
\dans{n}{\newpage}
\end{bloc}%bn
%
%
%%%\begin{bframe}{Loi exponentielle}
%%%
%%%La loi exponentielle est utilisée lorsqu'on s'intéresse au temps nécessaire avant qu'un événement se réalise. 
%%%
%%%Par exemple, dans l'étude des files d'attentes.
%%%\end{bframe}
%
%
\subsection{Loi normale}

\begin{bloc}{bimn}
\begin{defi}[Loi normale]{defLoiNormale}
Une variable aléatoire continue \(X\) suit une \defTerme{loi normale} de paramètres \(\mu\) et \(\sigma^2\), noté \(X\sim N(\mu ; \sigma^2)\), si sa fonction de densité est donnée par
\[
f(x) = \dfrac{1}{\sigma\sqrt{2\pi}}\ e^{-\frac{1}{2} \paren*{\frac{x-\mu}{\sigma}}^2} \text{ .}
\]
\end{defi}
\end{bloc}%bimn
%
%
%%%\begin{bloc}{bn}
%%%\begin{exemple}
%%%
%%%\QR{Calculer l'espérance et la variance d'une variable aléatoire \(X\) telle que\dans{n}{\\} \(X \sim N(0; 1)\).}{\(\E(X) = 0\) et \(\Var(x) = 1\)}[\vfilll \newpage]
%%%\end{exemple}
%%%\end{bloc}%bn


\begin{bloc}{bimn}[Propriétés]

Si \(X \sim N(\mu; \sigma^2)\), alors \(\E(X) = \mu\) et \(\Var(X)=\sigma^2\).
\end{bloc}%bimn


\begin{bloc}{bimn}[Graphiquement]

Graphiquement, la fonction de densité de la loi normale ressemble à une cloche.
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_distributions_loi_normale.pdf}
\end{center}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Soit \(X \sim N(0; 1)\).}
\debutSousQ[beaOpt=<+- |alert@+>, itSep=\dans{b}{0.4cm}]
\QR{Représenter graphiquement \(P(0 < X < 2)\).}{\begin{center}
\includegraphics[scale=0.5]{Tikz/Images/chap_distributions_normale_aire_entre_0_et_2.pdf}
\end{center}}[\vfill]
\QR{Calculer \(P(0 < X < 2)\).}{\(\intd{0}{2}{\dfrac{1}{\sqrt{2\pi}}\ e^{-\frac{1}{2} x^2}}{x}\)}[\vfill \newpage]
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}[Calcul de probabilité]

Pour évaluer l'intégrale, nous avons recours à des méthodes numériques. \pause
\begin{liste}[beaOpt=<+- |alert@+>, itSep=\dans{b}{0.4cm}]
\item On pourrait calculer des aires de rectangles inscrits ou circonscrits. 
\item On pourrait utiliser la méthode de Simpson.
\item On pourrait utiliser le développement en série de Taylor.
\item On pourrait utiliser bien d'autres méthodes.
\end{liste}
\end{bloc}%bn
%
%
\begin{bloc}{bn}[Table de loi normale]

Si le calcul doit être fait à la main, nous utilisons une table de loi normale. Celle-ci donne des probabilités précalculées. \dans{b}{\\[0.5cm] \pause Combien y a-t-il de lois normales?}
\end{bloc}%bn

\begin{bloc}{bimn}
\begin{defi}[Loi normale centrée réduite]{defLoiNormaleCentreeReduite}
Lorsqu'une variable aléatoire suit une loi normale d'espérance 0 et de variance 1, on dit qu'elle suit une \defTerme{loi normale centrée réduite}.
\end{defi}

\dans{b}{\pause}
On trouve une table de la loi normale centrée réduite à la page T-11 du manuel.
\end{bloc}%bimn


\begin{bloc}{bn}
\begin{exemple}

\Q{Soit \(Z \sim N(0; 1)\).}
\debutSousQ[beaOpt=<+- |alert@+>, itSep=\dans{b}{0.4cm}]
\QR{Évaluer \(P(0< Z < 1,5)\).}{\(0,4332\)}[\vfill \newpage]
\QR{Évaluer \(P(Z < 0,5)\).}{\(0,6915\)} \dans{n}{\vfill}
\QR{Trouver \(k\) si $P(0 < Z < k) = 0,3686\).}{\(k=1,12\)} \dans{n}{\vfill \vfill \newpage}
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\Q{Soit \(Z \sim N(0; 1)\).}
\debutSousQ[largeurQ=7cm, itSep=\dans{b}{0.3cm}]
\QR{Évaluer \(P(0< Z < 2)\).}{\(0,4772\)}[\vfill]
\QR{Évaluer \(P(Z > 2)\).}{\(0,0228\)}[\vfill]
\QR{Évaluer \(P(Z < -1)\).}{\(0,1587\)}[\vfill \newpage]
\QR{Évaluer \(P(-1 < Z < 1)\).}{\(0,6826\)}[\vfill]
\QR{Évaluer \(P(Z < 1)\).}{\(0,8413\)}[\vfill]
\QR{Trouver \(k\) si $P(0 < Z < k) = 0,4793\).}{\(k = 2,04\)}[\vfill \newpage]
\QR{Trouver \(k\) si $P(Z < k) = 0,1788\).}{\(k = -0,92\)}[\vfill]
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{prop}[]{propCoteZ}
Soit \(X \sim N(\mu; \sigma^2)\). Alors \[Z = \dfrac{X - \mu}{\sigma} \sim N(0; 1) \text{ .}\] \pause
De plus, \(P(a < X < b) = P(\frac{a-\mu}{\sigma} < Z < \frac{b-\mu}{\sigma})\).
\end{prop}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Soit \(X \sim N(15; 49)\). Calculer} 
\debutSousQ[beaOpt=<+- |alert@+>, itSep=\dans{b}{0.4cm}, largeurQ=7cm]
\QR{Évaluer \(P(15 < X < 22)\).}{\(0,3413\)}[\vfill \newpage]
\QR{Évaluer \(P(X > 25)\).}{\(0,0764\)}[\vfill]
\QR{Trouver \(k\) si \(P(k < X) = 0,5636\).}{\(13,88\)}[\vfill \newpage]
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\Q{La taille des citoyens d'une ville suit une loi normale de moyenne \qty{171}{cm} et d'écart type \qty{10}{cm}. On choisit une personne dans cette ville au hasard. Calculer la probabilité que la personne mesure...} 

\debutSousQ[largeurQ=7cm, itSep=\dans{b}{0.3cm}]
\QR{entre \qty{171}{cm} et \qty{179}{cm};}{\(0,2881\)}[\vfill]
\QR{moins de \qty{173}{cm};}{\(0,5793\)}[\vfill]
\QR{entre \qty{173}{cm} et \qty{176}{cm}.}{\(0,1122\)}[\vfill \newpage]
\QR{Quelle est la taille maximale des \qty{30}{\%} plus petites personnes?}{\qty{165,8}{cm}}[\vfill]
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{La durée d'une chanson qui joue à la radio suit une loi normale avec une durée moyenne de trois minutes et un écart type de 0,3 minute. Quelle est la probabilité que parmi dix de ces chansons choisies au hasard, quatre aient une durée de plus de 3,2 minutes?}{\(0,1476\)}[\vfill \vfill]
\end{exercice}
\end{bloc}%bn
\dans{n}{\newpage}
%
%
\begin{bloc}{bimn}
\begin{thm}[]{thmSommeVarNormale}
Si \(X_1\) et \(X_2\) sont deux variables aléatoires indépendantes et normalement distribuées avec \(X_1 \sim N(\mu_1, \sigma_1^2)\) et\dans{b}{\newline} \(X_2 \sim N(\mu_2, \sigma_2^2)\), alors la variable somme \(X_1 + X_2\) est aussi normalement distribuée. \[X_1+X_2 \sim N(\mu_1+\mu_2; \sigma_1^2 + \sigma_2^2)\]
\end{thm}

\pause
\dans{b}{Exemple d'utilisation : voir page 254 du manuel.}
\end{bloc}%bimn
\dans{n}{\vfill}
%
%
\devoirs[pageslecture={232 et 239 à 254}, pagesexe={255 et 256}, numsexe={1 - 2 - 7 - 8 - 9 - 10 - 11 - 12 - 13 - 14 - 15}]
%
%
\section{Théorème central limite}
%
%
\begin{bloc}{bimn}
\begin{thm}[Théorème central limite]{thmCentralLimite}
Soit \(X_1, X_2, \dots, X_n\) des variables aléatoires indépendantes et identiquement distribuées (i.i.d. : même fonction de densité ou loi de probabilité, même espérance \(\mu\) et même variance \(\sigma^2\)). 

Si \(n\) est grand, la variable somme \(Y = X_1 + X_2 + \dots + X_n\) est bien approximée par une loi normale, c'est-à-dire\dans{b}{\\} \(Y \approx N(n\mu;\, n\sigma^2)\).
\end{thm}
\pause
\dans{b}{Convergence visuelle : voir les pages 260 et 261 du manuel.}
\end{bloc}%bimn
%
%
\begin{bloc}{bm}
Somme de variables uniformes.
\begin{center}
\animategraphics[controls, scale=1]{3}{Asymptote/Somme de variables uniformes/_anim_somme_de_variables_uniformes-1+}{0}{18}
\end{center}
\end{bloc}%bm
%
%
\begin{bloc}{bm}
Somme de variables indicatrices avec probabilité \(\frac{1}{2}\).
\begin{center}
\animategraphics[controls, scale=1]{3}{Tikz/Animations/chap_distributions_anim_somme_indicatrices}{0}{6}
\end{center}
\end{bloc}%bm
%
%
\begin{bloc}{bm}
Somme de variables indicatrices avec probabilité \(\frac{1}{10}\).
\begin{center}
\animategraphics[controls, scale=1]{3}{Tikz/Animations/chap_distributions_anim_somme_indicatrices_p_petit}{0}{6}
\end{center}
\end{bloc}%bm
%
%
\begin{bloc}{bm}
Somme de variables qui suit une loi de Poisson.
\begin{center}
\animategraphics[controls, scale=1]{3}{Tikz/Animations/chap_distributions_anim_somme_poisson}{0}{11}
\end{center}
\end{bloc}%bm
%
%
\begin{bloc}{bm}
Somme de variables qui suit une loi exponentielle.
\begin{center}
\animategraphics[controls, scale=1]{3}{Tikz/Animations/chap_distributions_anim_somme_exponentielles_pas_normalisees}{0}{9}
\end{center}
\end{bloc}%bm
%
%
\dans{n}{\vfill \newpage}
%
%
%%%\subsection{Approximation d'une loi binomiale par une loi normale}
%%%%
%%%%
%%%\begin{bloc}{bn}
%%%\begin{prop}[Approximation de la loi binomiale]{propApproxBinNormale}
%%%Soit \(n\in\mathbb{N}^*\), \(p\in ]0, 1[\) et \(q=1-p\). Sous les hypothèses \(n\geq 30\), \(np\geq 5\) et \(nq\geq 5\), la variable aléatoire\dans{b}{\\} \(X\sim B(n; p)\) peut être approximée par la variable aléatoire \(Y\sim N(np; npq)\).
%%%\end{prop}
%%%\end{bloc}%bn
%%%%
%%%%
%%%\begin{bloc}{bn}
%%%\begin{exemple}
%%%\QR{Soit \(X \sim B(100; 0,4)\). Calculer \(P(X = 42)\).}{Environ \(0,0742\).} \dans{n}{\vfill}
%%%\end{exemple}
%%%\end{bloc}%bn
%%%%
%%%%
%%%\begin{bloc}{bn}[Remarque]
%%%
%%%Comme la fonction de densité d'une loi normale couvre toutes les valeurs réelles et que cela ne correspond pas aux valeurs possibles d'une variable aléatoire discrète, il faut faire une \textbf{correction de continuité} lorsqu'on approxime une somme de variables aléatoires discrètes. \pause
%%%
%%%Pour la loi binomiale, cette correction consiste à ajouter \(0,5\) à droite et à retrancher \(0,5\) à gauche de n'importe quelle valeur entière. \dans{n}{\newpage}
%%%\end{bloc}%bn
%%%%
%%%%
%%%\begin{bloc}{bn}
%%%\begin{exemple}
%%%
%%%\Q{Soit \(X \sim B(200; \frac{3}{10})\). Évaluer les probabilités suivantes.}
%%%\debutSousQ[beaOpt=<+- |alert@+>, itSep=\dans{b}{0.4cm}, largeurQ=7cm]
%%%\QR{\(P(X \leq 50)\)}{\(0,0708\)}[\vfill]
%%%\QR{\(P(55 \leq X < 65)\)}{\(0,5572\)}[\vfill \newpage]
%%%\finSousQ
%%%\end{exemple}
%%%\end{bloc}%bn
%%%%
%%%%
%%%\begin{bloc}{bn}
%%%\begin{exercice}
%%%
%%%\QR{Dans un cégep de \num{3500} étudiants, quelle est (approximativement) la probabilité qu'au moins 10 étudiants soient nés le jour de l'An? (Supposer qu'une année compte 365 jours.)}{\(0,5120\)}[\vfill]
%%%\end{exercice}
%%%\end{bloc}%bn
%%%%
%%%%
%%%\devoirs[pageslecture={258 à 264}, pageexe={265}, numsexe={1 - 2 - 3 - 4}]
%%%\dans{n}{\newpage}
