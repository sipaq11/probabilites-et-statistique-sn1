Vous trouverez ci-dessous le matériel, en développement, nécessaire pour l'enseignement du futur cours collégial de probabilités et statistique en sciences de la nature (201-SN1-05).

L'élaboration de ce matériel, par Simon Paquette et Pascal Turbis, a débuté en 2023. Environ la moitié du manuel est présentement écrit. Une révision scientifique est en cours pour ce travail. La suite devrait être rédigée en 2024. Ce projet est appuyé par le [Cégep Édouard-Montpetit](https://www.cegepmontpetit.ca/) et a été soutenu financièrement par la [Fabrique REL](https://fabriquerel.org/) en 2023.


Le matériel sera constitué de cinq fichiers :

Un manuel numérique, au format PDF, optimisé pour la lecture à l'ordinateur avec l'application gratuite Acrobat Reader.

Un manuel numérique, au format PDF, optimisé pour la lecture sur un appareil mobile.

Des diapositives, au format PDF, pour les présentations en classe. Il est préférable d'utiliser l'application gratuite Acrobat Reader pour profiter pleinement des fonctionnalités.

Des notes de cours à compléter par les étudiants et les étudiantes. Le contenu des notes de cours est le même que celui des diapositives, mais adapté à un format papier lettre avec de l'espace pour les compléter.

Un document, au format PDF, qui contient exclusivement les séries d'exercices du manuel ainsi que leurs réponses. Ce document est offert aux élèves qui préfèrent ne pas avoir d'appareil électronique lorsqu'ils font des exercices.

Ces documents ont été créés à l'aide de la classe LaTeX [multidoc](https://sipaq11.gitlab.io/simon/post/2021-10-06-multidoc/).
